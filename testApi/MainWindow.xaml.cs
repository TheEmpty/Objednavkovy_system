﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using testApi.WebClient;
using testApi.Entities;
using ServiceStack;
using testApi.Database;
using System.IO;



namespace testApi
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int FilterID;
        private int price;
        private string name;
        private string description;
        private static Db _database;
        private int oid;
        private int iID;
        private int iiID;
        private string Ammount;
        private int amm;
        private int num;
        private int obID;
        private string Status;
        private string UserNameS;
        private string PasswordS;
        private int logged_user;
        //instance databáze
        public static Db Database
        {
            get
            {
                if (_database == null)
                {
                    var fileHelper = new FileHelper();
                    _database = new Db(fileHelper.GetLocalFilePath("SQLite.db3"));
                }
                return _database;
            }
        }

        public Rest rest = new Rest();
        public MainWindow()
        {
            InitializeComponent();
            if(logged_user == null)
            {
                user.IsSelected = true;
                prd.IsEnabled = false;
                objednavka.IsEnabled = false;
            }
            else
            {
                prd.IsEnabled = true;
                objednavka.IsEnabled = true;
            }
            Restart();
        }
        /*
                public async void saveTodos(Todos td)
                {
                    await Database.SaveTodosAsync(td);
                }
            */

        public void Restart()
        {
            List<Todos> apiTodos = rest.getTodos();
            myList.ItemsSource = apiTodos.OrderBy(t => t.id);
            foreach (Todos td in apiTodos)
            {
                //saveTodos(td);
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                string Fin = FilterIn.Text;
                FilterID = Int32.Parse(Fin);
            }
            catch (Exception dd)
            {
                Console.WriteLine("nastala neočekávaná chyba:" + dd);
            }
        }

        private void Name_TextChanged(object sender, TextChangedEventArgs e)
        {
            name = Name.Text;
        }

        private void sID2_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sID2.Text != " ")
            {
                iID = Int32.Parse(sID2.Text);
            }
        }
        private void id_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (id.Text != " ")
            {
                iiID = Int32.Parse(id.Text);
            }
        }

        private void oID_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (oID.Text != "")
            {
                oid = Int32.Parse(oID.Text);
            }
        }

        private void Description_TextChanged(object sender, TextChangedEventArgs e)
        {
            description = Description.Text;
        }

        private void Price_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                string Pri = Price.Text;
                price = Int32.Parse(Pri);
            }
            catch (Exception dd)
            {
                Console.WriteLine("nastala neočekávaná chyba:" + dd);
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //  List<Todos> filtered_result = _database.Filterer(FilterID).Result;
            // myList.ItemsSource = _database.Filterer(FilterID).Result;
            List<Todos> apiTodos = rest.getTodos();
            myList.ItemsSource = apiTodos.OrderBy(t => t.id);

        }
        private void Filter_Click(object sender, RoutedEventArgs e)
        {
            //  List<Todos> filtered_result = _database.Filterer(FilterID).Result;
            // myList.ItemsSource = _database.Filterer(FilterID).Result;
            if (price != 0)
            {
                List<Todos> apiTodos = rest.getByPrice(price);
                myList.ItemsSource = apiTodos.OrderBy(t => t.id);
            }
            else if (FilterID != 0)
            {
                List<Todos> apiTodos = rest.getByCategory(FilterID);
                myList.ItemsSource = apiTodos.OrderBy(t => t.id);
            }
            else
            { 
                List<Todos> apiTodos = rest.getTodos();
                myList.ItemsSource = apiTodos.OrderBy(t => t.id);
            }

        }
        private void Write_Click(object sender, RoutedEventArgs e)
        {
            //  List<Todos> filtered_result = _database.Filterer(FilterID).Result;
            // myList.ItemsSource = _database.Filterer(FilterID).Result;
            List<Todos> apiTodos = rest.postWrite(description, name, FilterID, price);
            myList.ItemsSource = apiTodos.OrderBy(t => t.id);

        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
           // List<Todos> filtered_result = _database.Filterer(FilterID).Result;
         //   myList.ItemsSource = _database.Filterer(FilterID).Result;
            List<Todos> apiTodos = rest.postDelete(iID);
            Restart();
            //myList.ItemsSource = apiTodos.OrderBy(t => t.id);

        }


        private void myList_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            FilterIn.Text = FilterID.ToString();
            Name.Text = e.OriginalSource.ToString();
        }

        private void Update_Click(object sender, RoutedEventArgs e)
        {
            //  List<Todos> filtered_result = _database.Filterer(FilterID).Result;
            // myList.ItemsSource = _database.Filterer(FilterID).Result;

            List<Todos> apiTodos = rest.postUpdate(description, name, FilterID, price, iID);
            Restart();
          //  myList.ItemsSource = apiTodos.OrderBy(t => t.id);

        }

        private void myList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListView neco = (ListView)sender;
            Todos pomg = (Todos)neco.SelectedItem;
            if ((Todos)pomg == null)
            {
                Restart();
            }
            else
            {

                //sID = neco.SelectedItem.ToString();
                iID = pomg.id;
                FilterID = pomg.category_id;
                description = pomg.description;
                name = pomg.name;
                price = pomg.price;

                FilterIn.Text = FilterID.ToString();
                Description.Text = description;
                Name.Text = name;
                Price.Text = price.ToString();
                sID2.Text = iID.ToString();
            }
        }

        private void myObjednavka_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Do nothing
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (objednavka.IsSelected)
            {
                List<Cart> apiTodos = rest.getObjednavka(logged_user);
                myObjednavka.ItemsSource = apiTodos.OrderBy(t => t.ID);

                List<Cart> apiTodos1 = rest.getObjednavkaAll();
                allObjednavka.ItemsSource = apiTodos1.OrderBy(t => t.ID);
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            List<User> apiTodos = rest.getUser(UserNameS, PasswordS, logged_user);
        }

        private void UserName_TextChanged(object sender, TextChangedEventArgs e)
        {
            UserNameS = UserName.Text;
        }

        private void Password_TextChanged(object sender, TextChangedEventArgs e)
        {
           // PasswordS = Password.Text;
        }

        private void Ammount_TextChanged(object sender, TextChangedEventArgs e)
        {
            Ammount = ammount.Text;
            if (Ammount != " ")
            {
                amm = Int32.Parse(Ammount);
            }
        }

        private void Mnozstvi_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (mnozstvi.Text != "")
            {
                num = Int32.Parse(mnozstvi.Text);
            }
        }
        private void IDobj_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (id_obj.Text != "")
            {
                obID = Int32.Parse(id_obj.Text);
            }
        }
        private void Status_TextChanged(object sender, TextChangedEventArgs e)
        {
            Status = status.Text;
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            UserName.Text = " ";
            Password.Password = " ";
            sID2.Text = " ";
            Price.Text = " ";
            Description.Text = " ";
            Name.Text = " ";
            FilterIn.Text = " ";
            ammount.Text = " ";
            id.Text = " ";
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            List<User> apiTodos = rest.LoginUser(UserNameS, PasswordS);
            //logged_user = apiTodos;
 
            logged_user = apiTodos[0].id;
            userData.ItemsSource = apiTodos;
            UserName.Text = "";
            Password.Password = "";
        }

        private void ObjUpdate_Click(object sender, RoutedEventArgs e)
        {
            if(num != 0)
            {
                List<Cart> apiTodos = rest.objUpdateMnozstvi(obID, num);
                Restart();
            }
            else
            {
                List<Cart> apiTodos = rest.objUpdateStatus(obID, Status);
                Restart();
            }
           
        }
        private void ObjDelete_Click(object sender, RoutedEventArgs e)
        {
            List<Cart> apiTodos = rest.ObjDelete(obID);
            Restart();
        }
            private void Register_Click(object sender, RoutedEventArgs e)
        {
            List<User> apiTodos = rest.RegisterUser(UserNameS, PasswordS);
            Notice.Content = "Nyní se můžete přihlásit";
        }

        private void userData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Do nothing...
        }

        private void Objednat_Click(object sender, RoutedEventArgs e)
        {
            if (oid == 0)
            {
                List<Cart> apiTodos = rest.buyMeItem(iiID, amm, logged_user);
                // myObjednavka.ItemsSource = apiTodos.OrderBy(t => t.ID);
            }
            else
            {
                List<Cart> apiTodos = rest.editObjednavka(oid, iiID, amm);
            }

        }

        private void Password_PasswordChanged(object sender, RoutedEventArgs e)
        {
            PasswordS = Password.Password;
        }
    } }
