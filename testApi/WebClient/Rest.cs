﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testApi.Entities;
using ServiceStack;
using RestSharp;

namespace testApi.WebClient
{
   // public string adresa = "https://student.sps-prosek.cz/api2";
  public class Rest
  {
    public IServiceClient client = new JsonServiceClient("https://student.sps-prosek.cz").WithCache();
    public List<Todos> getTodos()
    {
            var client = new RestClient("https://student.sps-prosek.cz/~kocvaja14/");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("api2/", Method.POST);
            request.AddParameter("action", ""); // replaces matching token in request.Resource
    
            // execute the request
            IRestResponse response = client.Execute(request);


            /*  response2.AddParam("action", "write");
              response2.AddParam("description", description);
              response2.AddParam("category_id", category);
              response2.AddParam("name", name);
              response2 = client.Post<string>("/~kocvaja14/api2/index.php");*/

            try
            {
                var content = response.Content.FromJson<List<Todos>>(); // raw content as string
                return content;
            }
            catch (System.Net.WebException ex)
            {
                List<Todos> tdl = new List<Todos>();
                tdl.Add(new Todos { description = "error message: " + ex.Message });
                return tdl;
            }
    }

        public List<Cart> getCart()
        {

            var client = new RestClient("https://student.sps-prosek.cz/~kocvaja14/");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("api2/", Method.POST);
            request.AddParameter("action", "cart"); // replaces matching token in request.Resource
            request.AddParameter("id_users", "1");
            request.AddParameter("table", "objednavky");

            // execute the request
            IRestResponse response = client.Execute(request);


            /*  response2.AddParam("action", "write");
              response2.AddParam("description", description);
              response2.AddParam("category_id", category);
              response2.AddParam("name", name);
              response2 = client.Post<string>("/~kocvaja14/api2/index.php");*/

            try
            {
                var content = response.Content.FromJson<List<Cart>>(); // raw content as string
                return content;
            }
            catch (System.Net.WebException ex)
            {
                List<Cart> td2 = new List<Cart>();
             //   td2.Add(new Cart { description = "error message: " + ex.Message });
                return td2;
            }
        }
        public List<Cart> getObjednavky()
        {

            var client = new RestClient("https://student.sps-prosek.cz/~kocvaja14/");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("api2/", Method.POST);
            request.AddParameter("action", "objednavky"); // replaces matching token in request.Resource
            request.AddParameter("id_users", "1");
            request.AddParameter("table", "");

            // execute the request
            IRestResponse response = client.Execute(request);


            /*  response2.AddParam("action", "write");
              response2.AddParam("description", description);
              response2.AddParam("category_id", category);
              response2.AddParam("name", name);
              response2 = client.Post<string>("/~kocvaja14/api2/index.php");*/

            try
            {
                var content = response.Content.FromJson<List<Cart>>(); // raw content as string
                return content;
            }
            catch (System.Net.WebException ex)
            {
                List<Cart> td2 = new List<Cart>();
                //   td2.Add(new Cart { description = "error message: " + ex.Message });
                return td2;
            }
        }

        public List<Cart> getObjednavkaAll()
        {

            var client = new RestClient("https://student.sps-prosek.cz/~kocvaja14/");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("api2/", Method.POST);
            request.AddParameter("action", "objednavka_all"); // replaces matching token in request.Resource

            // execute the request
            IRestResponse response = client.Execute(request);


            /*  response2.AddParam("action", "write");
              response2.AddParam("description", description);
              response2.AddParam("category_id", category);
              response2.AddParam("name", name);
              response2 = client.Post<string>("/~kocvaja14/api2/index.php");*/

            try
            {
                var content = response.Content.FromJson<List<Cart>>(); // raw content as string
                return content;
            }
            catch (System.Net.WebException ex)
            {
                List<Cart> td2 = new List<Cart>();
                //   td2.Add(new Cart { description = "error message: " + ex.Message });
                return td2;
            }
        }

        public List<Cart> getObjednavka(int id)
        {

            var client = new RestClient("https://student.sps-prosek.cz/~kocvaja14/");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("api2/", Method.POST);
            request.AddParameter("action", "objednavka"); // replaces matching token in request.Resource
            request.AddParameter("id_users", id);

            // execute the request
            IRestResponse response = client.Execute(request);


            /*  response2.AddParam("action", "write");
              response2.AddParam("description", description);
              response2.AddParam("category_id", category);
              response2.AddParam("name", name);
              response2 = client.Post<string>("/~kocvaja14/api2/index.php");*/

            try
            {
                var content = response.Content.FromJson<List<Cart>>(); // raw content as string
                return content;
            }
            catch (System.Net.WebException ex)
            {
                List<Cart> td2 = new List<Cart>();
                //td2.Add(new Cart { description = "error message: " + ex.Message });
                return td2;
            }
        }

        public List<Cart> ObjDelete(int id)
        {

            var client = new RestClient("https://student.sps-prosek.cz/~kocvaja14/");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("api2/", Method.POST);
            request.AddParameter("action", "deleteObjednavka"); // replaces matching token in request.Resource
            request.AddParameter("id", id);
            // execute the request
            IRestResponse response = client.Execute(request);


            /*  response2.AddParam("action", "write");
              response2.AddParam("description", description);
              response2.AddParam("category_id", category);
              response2.AddParam("name", name);
              response2 = client.Post<string>("/~kocvaja14/api2/index.php");*/

            try
            {
                var content = response.Content.FromJson<List<Cart>>(); // raw content as string
                return content;
            }
            catch (System.Net.WebException ex)
            {
                List<Cart> td2 = new List<Cart>();
                //td2.Add(new Cart { description = "error message: " + ex.Message });
                return td2;
            }
        }

        public List<User> LoginUser(string name, string password)
        {
            var client = new RestClient("https://student.sps-prosek.cz/~kocvaja14/");
            var request = new RestRequest("api2/", Method.POST);
            request.AddParameter("action", "login");
            request.AddParameter("name", name);
            request.AddParameter("pswd", password);

            IRestResponse response = client.Execute(request);
            try
            {
                List<User> content = response.Content.FromJson<List<User>>();
                return content;
            }
            catch(System.Net.WebException ex)
            {
                List<User> td2 = new List<User>();
                td2.Add(new User { name = "error message: " + ex.Message });
                return td2;
            }

        }



        public List<User> RegisterUser(string name, string password)
        {
            var client = new RestClient("https://student.sps-prosek.cz/~kocvaja14/");
            var request = new RestRequest("api2/", Method.POST);
            request.AddParameter("action", "register");
            request.AddParameter("name", name);
            request.AddParameter("pswd", password);

            IRestResponse response = client.Execute(request);
            try
            {
                var content = response.Content.FromJson<List<User>>();
                return content;
            }
            catch (System.Net.WebException ex)
            {
                List<User> td2 = new List<User>();
                td2.Add(new User { name = "error message: " + ex.Message });
                return td2;
            }
        }

        public List<User> getUser(string name, string password, int uID)
        {
            var client = new RestClient("https://student.sps-prosek.cz/~kocvaja14/");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("api2/", Method.POST);
            request.AddParameter("action", "updateuser"); // replaces matching token in request.Resource
            request.AddParameter("name", name); // adds to POST or URL querystring based on Method
            request.AddParameter("pswd", password); // adds to POST or URL querystring based on Method
            request.AddParameter("id_users", uID);


            // execute the request
            IRestResponse response = client.Execute(request);


            /*  response2.AddParam("action", "write");
              response2.AddParam("description", description);
              response2.AddParam("category_id", category);
              response2.AddParam("name", name);
              response2 = client.Post<string>("/~kocvaja14/api2/index.php");*/

            try
            {
                var content = response.Content.FromJson<List<User>>(); // raw content as string
                return content;
            }
            catch (System.Net.WebException ex)
            {
                List<User> td2 = new List<User>();
                td2.Add(new User { name = "error message: " + ex.Message });
                return td2;
            }
        }

        internal List<Todos> giveMeItem(int iID)
        {
            throw new NotImplementedException();
        }

        public List<Todos> getByCategory(int id)
        {

            var client = new RestClient("https://student.sps-prosek.cz/~kocvaja14");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);
            var request = new RestRequest("api2/", Method.POST);
            request.AddParameter("action", "category"); // replaces matching token in request.Resource
            request.AddParameter("category_id", id); // adds to POST or URL querystring based on Method

            // execute the request
            IRestResponse response = client.Execute(request);


            /*  response2.AddParam("action", "write");
              response2.AddParam("description", description);
              response2.AddParam("category_id", category);
              response2.AddParam("name", name);
              response2 = client.Post<string>("/~kocvaja14/api2/index.php");*/

            try
            {
                var content = response.Content.FromJson<List<Todos>>(); // raw content as string
                return content;
            }
            catch (System.Net.WebException ex)
            {
                List<Todos> td2 = new List<Todos>();
                td2.Add(new Todos { description = "error message: " + ex.Message });
                return td2;
            }
            /*
            try
            {
                string response = client.Get<string>("/~kocvaja14/api2/index.php?action=category&category_id="+id);
                return response.FromJson<List<Todos>>();
            }
            catch(System.Net.WebException ex)
            {
                List<Todos> td2 = new List<Todos>();
                td2.Add(new Todos { description = "error message: " + ex.Message });
                return td2;
            }*/
        }

    public List<Todos> getByPrice(int price)
    {
            var client = new RestClient("https://student.sps-prosek.cz/~kocvaja14/");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("api2/", Method.POST);
            request.AddParameter("action", "price"); // replaces matching token in request.Resource
            request.AddParameter("price", price); // adds to POST or URL querystring based on Method

            // execute the request
            IRestResponse response = client.Execute(request);


            /*  response2.AddParam("action", "write");
              response2.AddParam("description", description);
              response2.AddParam("category_id", category);
              response2.AddParam("name", name);
              response2 = client.Post<string>("/~kocvaja14/api2/index.php");*/

            try
            {
                var content = response.Content.FromJson<List<Todos>>(); // raw content as string
                return content;
            }
            catch (System.Net.WebException ex)
            {
            List<Todos> td2 = new List<Todos>();
            td2.Add(new Todos { description = "error message: " + ex.Message });
            return td2;
        }
    }

        public List<Cart> buyMeItem(int pID, int ammount, int uID)
        {
            var client = new RestClient("https://student.sps-prosek.cz/~kocvaja14/");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("api2/", Method.POST);
            request.AddParameter("action", "write_objednavky"); // replaces matching token in request.Resource
            request.AddParameter("id_objednavky", pID);
            request.AddParameter("mnozstvi", ammount);
            request.AddParameter("id_users", uID);


            // execute the request
            IRestResponse response = client.Execute(request);


            /*  response2.AddParam("action", "write");
              response2.AddParam("description", description);
              response2.AddParam("category_id", category);
              response2.AddParam("name", name);
              response2 = client.Post<string>("/~kocvaja14/api2/index.php");*/

            try
            {
                var content = response.Content.FromJson<List<Cart>>(); // raw content as string
                return content;
            }
            catch (System.Net.WebException ex)
            {
                List<Cart> td2 = new List<Cart>();
                td2.Add(new Cart { description = "error message: " + ex.Message });
                return td2;
            }
        }

        public List<Cart> objUpdateMnozstvi(int ID, int ammount)
        {
            var client = new RestClient("https://student.sps-prosek.cz/~kocvaja14/");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("api2/", Method.POST);
            request.AddParameter("action", "reObjednavka"); // replaces matching token in request.Resource
            request.AddParameter("id", ID);
            request.AddParameter("mnozstvi", ammount);


            // execute the request
            IRestResponse response = client.Execute(request);


            /*  response2.AddParam("action", "write");
              response2.AddParam("description", description);
              response2.AddParam("category_id", category);
              response2.AddParam("name", name);
              response2 = client.Post<string>("/~kocvaja14/api2/index.php");*/

            try
            {
                var content = response.Content.FromJson<List<Cart>>(); // raw content as string
                return content;
            }
            catch (System.Net.WebException ex)
            {
                List<Cart> td2 = new List<Cart>();
                td2.Add(new Cart { description = "error message: " + ex.Message });
                return td2;
            }
        }

        public List<Cart> objUpdateStatus(int ID, string Status)
        {
            var client = new RestClient("https://student.sps-prosek.cz/~kocvaja14/");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("api2/", Method.POST);
            request.AddParameter("action", "reObjednavky"); // replaces matching token in request.Resource
            request.AddParameter("id", ID);
            request.AddParameter("status", Status);


            // execute the request
            IRestResponse response = client.Execute(request);


            /*  response2.AddParam("action", "write");
              response2.AddParam("description", description);
              response2.AddParam("category_id", category);
              response2.AddParam("name", name);
              response2 = client.Post<string>("/~kocvaja14/api2/index.php");*/

            try
            {
                var content = response.Content.FromJson<List<Cart>>(); // raw content as string
                return content;
            }
            catch (System.Net.WebException ex)
            {
                List<Cart> td2 = new List<Cart>();
                td2.Add(new Cart { description = "error message: " + ex.Message });
                return td2;
            }
        }

        public List<Cart> editObjednavka(int oID, int pID, int ammount)
        {
            var client = new RestClient("https://student.sps-prosek.cz/~kocvaja14/");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("api2/", Method.POST);
            request.AddParameter("action", "edit_objednavka"); // replaces matching token in request.Resource
            request.AddParameter("id", pID);
            request.AddParameter("id_objednavky", oID);
            request.AddParameter("mnozstvi", ammount);


            // execute the request
            IRestResponse response = client.Execute(request);


            /*  response2.AddParam("action", "write");
              response2.AddParam("description", description);
              response2.AddParam("category_id", category);
              response2.AddParam("name", name);
              response2 = client.Post<string>("/~kocvaja14/api2/index.php");*/

            try
            {
                var content = response.Content.FromJson<List<Cart>>(); // raw content as string
                return content;
            }
            catch (System.Net.WebException ex)
            {
                List<Cart> td2 = new List<Cart>();
                td2.Add(new Cart { description = "error message: " + ex.Message });
                return td2;
            }
        }

        public List<Todos> getByID(int id)
        {
            var client = new RestClient("https://student.sps-prosek.cz/~kocvaja14/");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("api2/", Method.POST);
            request.AddParameter("action", "id"); // replaces matching token in request.Resource
            request.AddParameter("id", id); // adds to POST or URL querystring based on Method

            // execute the request
            IRestResponse response = client.Execute(request);


            /*  response2.AddParam("action", "write");
              response2.AddParam("description", description);
              response2.AddParam("category_id", category);
              response2.AddParam("name", name);
              response2 = client.Post<string>("/~kocvaja14/api2/index.php");*/

            try
            {
                var content = response.Content.FromJson<List<Todos>>(); // raw content as string
                return content;
            }
            catch (System.Net.WebException ex)
            {
                List<Todos> td2 = new List<Todos>();
                td2.Add(new Todos { description = "error message: " + ex.Message });
                return td2;
            }
        }

        public List<Todos> postWrite(string description, string name, int category, int price)
        {
                var client = new RestClient("https://student.sps-prosek.cz/~kocvaja14/");
                // client.Authenticator = new HttpBasicAuthenticator(username, password);

                var request = new RestRequest("api2/", Method.POST);
                request.AddParameter("action", "write"); // replaces matching token in request.Resource
                request.AddParameter("name", name); // adds to POST or URL querystring based on Method
                request.AddParameter("description", description); // adds to POST or URL querystring based on Method
                request.AddParameter("category_id", category); // adds to POST or URL querystring based on Method
                request.AddParameter("price", price); // adds to POST or URL querystring based on Method

                // execute the request
                IRestResponse response = client.Execute(request);


            /*  response2.AddParam("action", "write");
              response2.AddParam("description", description);
              response2.AddParam("category_id", category);
              response2.AddParam("name", name);
              response2 = client.Post<string>("/~kocvaja14/api2/index.php");*/

            try
            {
                var content = response.Content.FromJson<List<Todos>>(); // raw content as string
                return content;
            }
            catch (System.Net.WebException ex)
            {
                List<Todos> td2 = new List<Todos>();
                td2.Add(new Todos { description = "error message: " + ex.Message });
                return td2;
            }
        }

        public List<Todos> postUpdate(string description, string name, int category, int price, int id)
        {
            var client = new RestClient("https://student.sps-prosek.cz/~kocvaja14/");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("api2/", Method.POST);
            request.AddParameter("action", "update"); // replaces matching token in request.Resource
            request.AddParameter("id", id);
            request.AddParameter("name", name); // adds to POST or URL querystring based on Method
            request.AddParameter("description", description); // adds to POST or URL querystring based on Method
            request.AddParameter("category_id", category); // adds to POST or URL querystring based on Method
            request.AddParameter("price", price); // adds to POST or URL querystring based on Method

            // execute the request
            IRestResponse response = client.Execute(request);


            /*  response2.AddParam("action", "write");
              response2.AddParam("description", description);
              response2.AddParam("category_id", category);
              response2.AddParam("name", name);
              response2 = client.Post<string>("/~kocvaja14/api2/index.php");*/

            try
            {
                var content = response.Content.FromJson<List<Todos>>(); // raw content as string
                return content;
            }
            catch (System.Net.WebException ex)
            {
                List<Todos> td2 = new List<Todos>();
                td2.Add(new Todos { description = "error message: " + ex.Message });
                return td2;
            }
        }

        public List<Todos> postDelete(int ID)
        {
            var client = new RestClient("https://student.sps-prosek.cz/~kocvaja14/");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("api2/", Method.POST);
            request.AddParameter("action", "delete"); // replaces matching token in request.Resource
            request.AddParameter("id", ID); // adds to POST or URL querystring based on Method
   

            // execute the request
            IRestResponse response = client.Execute(request);


            /*  response2.AddParam("action", "write");
              response2.AddParam("description", description);
              response2.AddParam("category_id", category);
              response2.AddParam("name", name);
              response2 = client.Post<string>("/~kocvaja14/api2/index.php");*/

            try
            {
                var content = response.Content.FromJson<List<Todos>>(); // raw content as string
                return content;
            }
            catch (System.Net.WebException ex)
            {
                List<Todos> td2 = new List<Todos>();
                td2.Add(new Todos { description = "error message: " + ex.Message });
                return td2;
            }

            /*
            string response = client.Post<string>("/~kocvaja14/api2/index.php");
            response.AddParam("action", "write");
            response.AddParam("id", ID);

            try
            {

                return response.FromJson<List<Todos>>();
            }
            catch (System.Net.WebException ex)
            {
                List<Todos> td2 = new List<Todos>();
                td2.Add(new Todos { description = "error message: " + ex.Message });
                return td2;
            }*/
        }
    }
}
